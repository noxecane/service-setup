import app
from app import {{cookiecutter.app_name}}_db, auth
from app.errors import BlogEntryExists, UnknownBlogEntry
from app.schemas import BlogEntry

from bson import ObjectId
from pylib.misc import search
from pylib.misc.search import Engine
from pylib.views import collection, entity, object_id_hook
from pylib.views.collection import Collection
from pylib.views.entity import Entity


{{cookiecutter.app_name}}_collection = Collection('/api/blogs', BlogEntry(), before=[auth])
{{cookiecutter.app_name}}_entity = Entity('/api/blogs', 'bid', BlogEntry(), before=[auth, object_id_hook('eid')])
blogsdb = gakp_db.blog_entries
search_engine = Engine()


def is_object_id(query):
    return len(query) == 24


@search.search_with(search_engine, search.regex_criteria(r'\d+', match=True))
def by_bid(query, user):
    return list(blogsdb.find({'bid': int(query)}))


@search.search_with(search_engine, is_object_id)
def by__id(query, user):
    return list(blogsdb.find({'_id': ObjectId(query)}))


@collection.create({{cookiecutter.app_name}}_collection)
def create_blog(blog, user, **kwargs):
    data, errors = inet_client.get('admin.users.get_by_id', dict(id=user['id']))
    blog['author'] = data
    return blogsdb.insert(blog)


@collection.search({{cookiecutter.app_name}}_collection)
def search_patient(query, user, **kwargs):
    return search.search(search_engine, query, user)


@entity.read({{cookiecutter.app_name}}_entity)
def read_blog(bid, user, **kwargs):
    blog = blogdb.find_one({'_id': bid})
    if not blog:
        raise UnknownBlogEntry()
    return blog


@entity.update({{cookiecutter.app_name}}_entity)
def update_blog(bid, blog, user, **kwargs):
    old_blog = read_blog(bid, user)
    # update old blog
    old_blog.update(blog)
    blogdb.update({'_id': bid}, {'$set': old_blog})
    return old_blog
